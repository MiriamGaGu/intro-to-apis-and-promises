const rest = superagent;
let flagsContainer = document.querySelector(".countries-container");

rest
  .get("https://restcountries.eu/rest/v2/all")

  .then(function(data) {
    const obj = data.body;
    console.log(obj);

    obj.forEach(function(element) {
      console.log(element.capital);
      // console.log(element.flag);
      // console.log(element.name);
      flagsContainer.innerHTML += `<div class="country-card">
                    <img class="country-flag" src=${element.flag} alt="flag">
                    <h4 class="country-name">${element.name}</h4>
                    <p class="country-capital">${element.capital}</p>
                </div>`;
    });
  });
